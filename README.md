# Francil’IN website

## Local dev

To test website and CMS, run in parallel:
- `hugo server`
- `npx netlify-cms-proxy-server` (this is used to access local filesystem as a remote Git repo using Netlify CMS). More info [here](https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository)

Admin (for content edition) is accessible via `/admin`

## Contributing

Git flow is inspired by [gitlab-flow](https://about.gitlab.com/blog/2014/09/29/gitlab-flow/)

### Local git configuration

To keep Git history clear, please **configure Git** to [disable fast forward when merging](https://git-scm.com/docs/merge-config#Documentation/merge-config.txt-mergeff):

- in your `~/.gitconfig`
    ```ini
    [merge]
        # I like to keep Git history clear, so I will manually add "--ff" to merge options when I want to fast  forward
        ff = false
    ```

**note**: if you don't want to change your global conf, you can configure it only for this repo using Git [`includeIf` conf key](https://git-scm.com/docs/git-config#_conditional_includes) (only available in [Git > 2.13.0](https://github.blog/2017-05-10-git-2-13-has-been-released/))


### Netlify deployment

#### Git branches

- master: current development branch
    - commits on this branch doesn't imply any deployment
- `netlify-dev` -> https://netlify-dev--francilin-website.netlify.com/
    - **dev instance** branch = used by developers to test instable features on Netlify instance.
    - every commits/merge on this branch will trigger a deployment to dev instance
- `netlify-preprod` -> https://netlify-preprod--francilin-website.netlify.com/
    - **preprod instance** branch = used to deploy stable features and let client validate
    - every commits/merge on this branch will trigger a deployment to preprod instance
- `netlify-prod`
    - **production instance** branch
    - should only be used to merge *feature branches* after they have been tested on preprod instance (via commit on *netlify-preprod* branch)

#### Hugo version

This website compiles correctly with Hugo v0.57.2.

The default Hugo version used by Netlify is different, so we pinned the version in `netlify.toml`.

To work locally you can download this specific version from https://github.com/gohugoio/hugo/releases/tag/v0.57.2

Do **not** change this version without testing those functionalities:
- services that are marked to be displayed on home page (via `featured: true`) are correctly displayed on the home page
