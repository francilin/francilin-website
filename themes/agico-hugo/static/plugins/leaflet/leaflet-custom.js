var mapCanvasElement = document.getElementById("map_canvas")
var $mapCanvas = $(mapCanvasElement)

if (mapCanvasElement) {
  var map = L.map(mapCanvasElement).setView(
    [
      $mapCanvas.attr("data-latitude"),
      $mapCanvas.attr("data-longitude"),
    ],
    13
  )
  L.tileLayer(decodeURIComponent($mapCanvas.attr("data-tiles-url")), {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  }).addTo(map)

  L.marker([
    $mapCanvas.attr("data-latitude"),
    $mapCanvas.attr("data-longitude"),
  ]).addTo(map)
}
