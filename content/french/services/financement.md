---
list: false
featured: true
title: Financement
categories:
  - title: Management
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
      eiusmod tempor incididunt ut labore et dolore magna aliqua. "
    image: /images/uploads/logo-4.png
  - title: Test
    description: bla bla
    image: /images/uploads/logo-5.png
numbers:
  enabled: true
  numbers_items:
    - count: 300
      name: Projets financés
description: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
  eiusmod tempor incididunt ut labore et dolore.
image: /images/uploads/financements.jpg
---


### Jobs Description

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur.

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
sed ut perspiciatis unde.


##### Responsibilities:

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
id est laborum sed ut perspiciatis unde.

* Talking with clients or account managers to discuss the business objectives and requirements of the job;
* Estimating the time required to complete the work and providing quotes for clients;
* Developing design briefs that suit the client's purpose;
* Thinking creatively to produce new ideas, concepts and developing interactive design;
* Presenting finalized ideas and concepts to clients or account managers;


##### Requirements:

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
id est laborum sed ut perspiciatis unde.

* Talking with clients or account managers to discuss the business objectives and requirements of the job;
* Estimating the time required to complete the work and providing quotes for clients;
* Developing design briefs that suit the client's purpose;
* Thinking creatively to produce new ideas, concepts and developing interactive design;
* Presenting finalized ideas and concepts to clients or account managers;
