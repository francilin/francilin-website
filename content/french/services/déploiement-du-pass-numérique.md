---
list: false
featured: true
title: "Déploiement du Pass numérique "
categories:
  - title: "Métropole du Grand Paris "
    description: "160 000 Pass numériques "
    image: /images/uploads/logo_metropole_du_grand_paris_2016.png
  - title: Département des Hauts-de-Seine
    description: "50 000 Pass numériques "
    image: /images/uploads/logo-92.png
numbers:
  enabled: true
  numbers_items:
    - count: 160000
      name: Pass financés par la Métropole du Grand Paris
description: Le Hub Francil'IN accompagne les territoires franciliens dans le
  déploiement du Pass numérique
image: /images/uploads/pass-numerique.png
---
Le Pass numérique est un dispositif d'inclusion numérique soutenu par l'Etat. Il se matérialise par des carnets de chèques permettant aux bénéficiaires d’accéder – dans des lieux préalablement qualifiés – à des services d’accompagnement numérique. En pratique, les personnes se voient remettre un carnet de pass par une structure locale (guichet de service public, associations, etc.) et peuvent ensuite se former aux usages numériques auprès d'un acteur de la médiation numérique du territoire